# buildup-android

Android 開発環境構築手順リポ

こちらでは、Android環境の構築手順をご案内します。


# 準備するもの

| プログラム      | バージョン(2019/11/08 現在)  |
|:----------------|-----------------------------:|
| [Android Studio](https://developer.android.com/studio)  | 3.5.2                        |
| [SourceTree](https://www.sourcetreeapp.com/)          | 3.2.6                       |
| [Gitlabアカウント](https://gitlab.com/users/sign_in)            | null                         |
| [JDK(Java Developer Kit)](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html) | 11.0.5 |




# 開発マシン
参考までに自宅マシンのスペックを書いておきます  

| 開発マシン | バージョン |
| ------ | ------ |
| OS | Windows 10 Pro x64 1903 |
| RAM | 16GB | 
| CPU | Intel Core i5-4590 | 
| HDD | 500GB | 

# ターゲットデバイス
|ターゲットデバイス|バージョン|
|:-----------------|---------:|
|エミュレータ | Android 7.1.1(Nougat) in Google APIs ARM 64 v8a  System Image|
|実機         | Android 7.1.1(Nougat) in ASUS Zenfone 4 Max|


こちらを確認したうえで、[こちら](setup_stanby.md)に続いてください。