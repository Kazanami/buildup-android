# buildup-android

Android 開発環境構築手順リポ

こちらでは、Android Studio のインストール手順をご案内します。

# まずはマシンの確認

VT-x(仮想化支援機能)が対応しているか、さらに必要条件を満たしているかを確認します。


# Android Studioが動作する最低条件
|項目|必須 | 推奨 |
|----|:----|:-----|
|OS  | Windows 7/8/10 | 左に同じく Android エミュレータを動かすには64bitOSが必要 |
|RAM | 4GB | 8GB |
|ストレージ | 2GB | 4GB (IDEに500MB + Android SDK と エミュレータイメージに 1.5GB)|
|画面| 1280x800以上| 左に同じく |

# 確認方法

共通
1. [VirtualChecker](https://openlibsys.org/)をダウンロードし、展開。
2. VirtualCheckerを実行し、下記のとおりになっていれば、Androidエミュレータを稼動できる。  
![VirtualChecker](https://gitlab.com/Kazanami/buildup-android/raw/images/VirtualChecker.PNG)

画像のとおりにEnabled/Supportedになっていない場合は、CPUの型番を調べ、仮想化支援機能があるか確認してください。

CPU 確認方法

(画像はWindows 7)

1. スタートメニュー -> コントロールパネル -> システムと進む
2. プロセッサの項目をGoogle などで検索 ([CPU名] 仕様)  
![System Info](https://gitlab.com/Kazanami/buildup-android/raw/images/System_info.PNG)
3. 該当CPUの仕様を見つけたら、【インテル バーチャライゼーション・テクノロジー(VT-x)】を探す  
![VT-x_check](https://gitlab.com/Kazanami/buildup-android/raw/images/VT-x_Check.PNG)  
4.A 【インテル バーチャライゼーション・テクノロジー(VT-x)】の項目が「はい」の場合、BIOSで無効化されている可能性あり。 設定は各自で調べてください。  
4.B 【インテル バーチャライゼーション・テクノロジー(VT-x)】の項目が「いいえ」の場合、実機で試すか、あきらめてください。  


確認できましたら、[こちら](setup.md)に進んでください