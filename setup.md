# buildup-android

Android 開発環境構築手順リポ

こちらでは、Android Studioのインストールをご案内します。

# JDKのダウンロード・インストール

1. [こちら](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html)にアクセスし、exeインストーラーをダウンロード。
2. ダウンロードが完了したら、画面の指示に従い、インストールしてください。


# Android Studio のダウンロード・インストールおよび日本語化

1. [こちら](https://developer.android.com/studio)にアクセスし、[DOWNLOAD ANDROID STUDIO]をクリック
2. ダウンロードが完了したら、画面の指示に従ってインストール(このとき、インストールパスを覚えておいてください。)
3. インストールが完了したら、[Start Android Studio]のチェックボックスをはずし、終了する
4. [こちら](https://mergedoc.osdn.jp/)にアクセスし、 Pleiadesプラグインをダウンロードする
5. zipをダウンロードしたら、展開し中にあるsetup.exeを実行
6. 実行したら「日本語化するアプリケーション」に先ほどのインストールパスをコピペし、「bin\studio64.exe」を指定する
7. 「日本語化する」をクリックし待つ。
8. 完了したら、Android Studioを起動する。
9. Google に情報を送信するか否か 聞いてくるので、送信しないを選択
10. 起動したらセットアップを進める。
11. コンポーネントをインストールする際、下に詳細表示のチェックボックスがありますのでそれを有効にした上で、下記の項目を選択してインストールしてください。  

・Intel x86 Emulator Accelerator (HAXM installer)  
・Android 7.1.1 (Nougat)の項目は下記をチェックしてください。  
* 	Google APIs ARM 64 v8a System Image  
* 	Android SDK Platform 25  
* 	Sources for Android 25  

・Android SDK Build-Tools (最新版)  
・Android SDK Platform-Tools (最新版)  
・NDK (最新版)  

12. インストールに時間がすごくかかりますので、その間にGitlabのセットアップをします。


# Gitlabのセットアップ

**こちらは任意です**

1. [こちら](https://gitlab.com/users/sign_in) からサインアップを行ってください。 これは無料で登録、および使用ができます。
2. サインアップが完了したら、「技術部」のノートにある、「グループリンク」をクリックしてください。
3. すると、Gitlabのグループが表示され、リポジトリの場所は何も表示されていないはずです。
4. 上にある、「アクセス権限をリクエストする」または「Request Access」があるはずなのでこれをクリック。
5. 私にリクエストが届きますので、承認します。 念のため、登録したユーザ名を伝えてくれるとこちらもやりやすいです。

Gitの設定に関してはこちらが結構詳しいので、こちらに沿いつつ設定をしてみてください。
[わかばちゃんと学ぶ Git使い方入門]( https://next.rikunabi.com/journal/tag/webdesign-manga/) - 湊川あい